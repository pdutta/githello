package net.pdutta.sandbox;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class Main {

    public static void main (String[] args) {
        Display display = new Display();
        final Shell shell = new Shell(display);

        Rectangle boundRect = new Rectangle(0, 0, 640, 480);
        shell.setBounds(boundRect);

        GridLayout layout = new GridLayout();
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.horizontalSpacing = 0;
        layout.verticalSpacing = 0;
        layout.numColumns = 3;

        shell.setText("hellogit - GUI Hello World");
        shell.setLayout(layout);
        shell.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Label label = new Label(shell, SWT.CENTER);
        label.setFont(new Font(shell.getDisplay(), "Segoe UI", 11, SWT.NORMAL));
        label.setText("Hello World!");
        label.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));

        shell.addShellListener(new ShellAdapter() {
            public void shellDeiconified (ShellEvent shellEvent) {
                super.shellDeiconified(shellEvent);
            }

            public void shellIconified (ShellEvent shellEvent) {
                super.shellIconified(shellEvent);
            }

            public void shellDeactivated (ShellEvent shellEvent) {
                super.shellDeactivated(shellEvent);
            }

            public void shellClosed (ShellEvent shellEvent) {
                System.out.println("Client Area: " + shell.getClientArea());
                super.shellClosed(shellEvent);
            }

            public void shellActivated (ShellEvent shellEvent) {
                int frameX = shell.getSize().x - shell.getClientArea().width;
                int frameY = shell.getSize().y - shell.getClientArea().height;
                shell.setSize(640 + frameX, 480 + frameY);
            }
        });

        shell.pack();
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) display.sleep();
        }
        display.dispose();
    }
}
